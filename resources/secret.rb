#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

property :bin, String, default: '/bin/docker'
property :secret, String, name_property: true
property :file, String, default: ''
property :content, String, default: ''

action :create do
  nr = new_resource
  unless already_created?(nr.secret)
    msg_error = 'Swarm: please set a file or content for secret creation'
    raise msg_error if nr.file.empty? && nr.content.empty?

    msg_error = 'Swarm: please set only file or content but not both'
    raise msg_error unless nr.file.empty? || nr.content.empty?

    converge_by("Create swarm #{nr.secret} secret service") do
      secret_from_file(nr.secret, nr.file) unless nr.file.empty?
      secret_from_content(nr.secret, nr.content) unless nr.content.empty?
    end
  end
end

def secret_from_file(name, file)
  shell_out!("#{bin} secret create  #{name} #{file}")
end

def secret_from_content(name, content)
  shell_out!("printf \"#{content}\" | #{bin} secret create #{name} -")
end

def already_created?(name)
  shell_out(
    <<-BASH
      #{bin} secret ls \
      --filter name=#{name} --format '{{.Name}}' | grep '^#{name}$'
    BASH
  ).exitstatus.zero?
end
