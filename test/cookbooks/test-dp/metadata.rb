name 'test-dp'
maintainer 'Make.org'
maintainer_email 'sre@make.org'
license 'Apache 2.0'
description 'Helper Cookbook to test Docker Platform'
long_description 'Helper Cookbook to test Docker Platform'
source_url 'https://gitlab.com/chef-platform/docker'
issues_url 'https://gitlab.com/chef-platform/docker-platform/issues'
version '1.0.0'

chef_version '>= 12.19'

supports 'centos', '>= 7.3'
